package local.strateg.tasktest;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;

import local.strateg.tasktest.asynctask.AsyncTask;
import local.strateg.tasktest.http.JsonForeigen;
import local.strateg.tasktest.http.interfaces.CurrencyHttp;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CurrencyMain extends AppCompatActivity {
    private Retrofit retrofit;
    private EditText editText;
    private TextView textView;
    private Button button;

    private class LoadCurrency implements LoaderManager.LoaderCallbacks<List<JsonForeigen>> {
        String name = "";
        LoadCurrency(String name){
            this.name = name;
        }

        @Override
        public Loader<List<JsonForeigen>> onCreateLoader(int id, Bundle args) {
            Loader<List<JsonForeigen>> loader = new AsyncTask(CurrencyMain.this, name);;
            return loader;
        }

        @Override
        public void onLoadFinished(Loader<List<JsonForeigen>> loader, List<JsonForeigen> data) {
            String str = "";
            try {
                ArrayList<String[]> arrayList = data.get(0).getRates().getStrings();
                for (int i = 0; i < arrayList.size(); i++)
                    str += arrayList.get(i)[0] + " = " + arrayList.get(i)[1] + "\n";
                textView.setText(str);
                getSupportLoaderManager().destroyLoader(1);
            }catch (Exception e)
            {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Неверный формат даты!", Toast.LENGTH_SHORT);
                toast.show();
                e.printStackTrace();
            }
        }

        @Override
        public void onLoaderReset(Loader<List<JsonForeigen>> loader) {

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency);
        editText = (EditText) findViewById(R.id.code);
        textView = (TextView) findViewById(R.id.ListCurrency);
        button = (Button) findViewById(R.id.butGetCurrency);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportLoaderManager().initLoader(1, null, new LoadCurrency(editText.getText().toString()));
            }
        });

    }
}
