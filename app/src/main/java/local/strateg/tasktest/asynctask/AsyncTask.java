package local.strateg.tasktest.asynctask;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import local.strateg.tasktest.http.JsonForeigen;
import local.strateg.tasktest.http.Rates;
import local.strateg.tasktest.http.interfaces.CurrencyHttp;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by STRATEG on 29.06.2017.
 */

public class AsyncTask<D> extends AsyncTaskLoader<List<JsonForeigen>>{
    //private List<CommentsModel> modelList;
    private Response<JsonElement> response;
    private String name;
    public JsonForeigen body;

    public AsyncTask(Context context, String name) {
        super(context);
        this.name = name;
    }

    public void setName(String str){
        name = str;
    }

    @Override
    public List<JsonForeigen> loadInBackground() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.fixer.io") //Базовая часть адреса
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();
        CurrencyHttp currencyHttp = retrofit.create(CurrencyHttp.class);
        Call<JsonElement> gsonCurrency = currencyHttp.getCurrency(name);
        try {
            response = gsonCurrency.execute();
        }catch (Exception e){
            e.printStackTrace();
        }
        if(response.code() == 200){
            //String body = response.body();
            JsonElement jsonElement = response.body();
            Gson gson = new GsonBuilder().create();
            try {
                body = gson.fromJson(jsonElement, JsonForeigen.class);
                /*modelList = gson.fromJson(response.body())
                        , new TypeToken<ArrayList<CommentsModel>>() {
                        }.getType());*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ArrayList<JsonForeigen> arrayList = new ArrayList<>();
        try {
            arrayList.add(body);
        }catch (Exception e){}
        return arrayList;
        /*try {
            response = Requests.getInstance().getComments(projectId,null).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(response.code() == 200){
            Gson gson = new GsonBuilder().create();
            try {
                modelList = gson.fromJson(ChangeJson.changeJsonArray(response.body())
                        , new TypeToken<ArrayList<CommentsModel>>() {
                        }.getType());
                // Log.d(TAG, "onResponse: "+models.get(2).getAuthor().getUserProfile().getFirstName());
            } catch (Exception e) {
            }
        }
        return modelList;*/
    }

    @Override
    public void deliverResult(List<JsonForeigen> data) {
        super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
        /*if(modelList != null){
            deliverResult(modelList);
        }
        if (takeContentChanged() || modelList == null) {
            forceLoad();
        }*/
    }
}
