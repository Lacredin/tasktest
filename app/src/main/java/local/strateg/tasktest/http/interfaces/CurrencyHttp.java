package local.strateg.tasktest.http.interfaces;

import com.google.gson.JsonElement;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by STRATEG on 29.06.2017.
 */

public interface CurrencyHttp {
    //http://api.fixer.io/latest?base=USD
    @GET("/latest")
    Call<JsonElement> getCurrency(@Query("base") String baseCurrency);
}
