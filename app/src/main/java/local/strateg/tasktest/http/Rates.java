package local.strateg.tasktest.http;

/**
 * Created by STRATEG on 29.06.2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Rates {
    @SerializedName("base")
    @Expose
    public String gbase;
    @SerializedName("date")
    @Expose
    public String gdate;
/*"base": "EUR",
        "date": "2017-06-29"*/

    @SerializedName("AUD")
    @Expose
    private Double aUD;
    @SerializedName("BGN")
    @Expose
    private Double bGN;
    @SerializedName("BRL")
    @Expose
    private Double bRL;
    @SerializedName("CAD")
    @Expose
    private Double cAD;
    @SerializedName("CHF")
    @Expose
    private Double cHF;
    @SerializedName("CNY")
    @Expose
    private Double cNY;
    @SerializedName("CZK")
    @Expose
    private Double cZK;
    @SerializedName("DKK")
    @Expose
    private Double dKK;
    @SerializedName("GBP")
    @Expose
    private Double gBP;
    @SerializedName("HKD")
    @Expose
    private Double hKD;
    @SerializedName("HRK")
    @Expose
    private Double hRK;
    @SerializedName("HUF")
    @Expose
    private Double hUF;
    @SerializedName("IDR")
    @Expose
    private Integer iDR;
    @SerializedName("ILS")
    @Expose
    private Double iLS;
    @SerializedName("INR")
    @Expose
    private Double iNR;
    @SerializedName("JPY")
    @Expose
    private Double jPY;
    @SerializedName("KRW")
    @Expose
    private Double kRW;
    @SerializedName("MXN")
    @Expose
    private Double mXN;
    @SerializedName("MYR")
    @Expose
    private Double mYR;
    @SerializedName("NOK")
    @Expose
    private Double nOK;
    @SerializedName("NZD")
    @Expose
    private Double nZD;
    @SerializedName("PHP")
    @Expose
    private Double pHP;
    @SerializedName("PLN")
    @Expose
    private Double pLN;
    @SerializedName("RON")
    @Expose
    private Double rON;
    @SerializedName("RUB")
    @Expose
    private Double rUB;
    @SerializedName("SEK")
    @Expose
    private Double sEK;
    @SerializedName("SGD")
    @Expose
    private Double sGD;
    @SerializedName("THB")
    @Expose
    private Double tHB;
    @SerializedName("TRY")
    @Expose
    private Double tRY;
    @SerializedName("USD")
    @Expose
    private Double uSD;
    @SerializedName("ZAR")
    @Expose
    private Double zAR;

    public Double getAUD() {
        return aUD;
    }

    public void setAUD(Double aUD) {
        this.aUD = aUD;
    }

    public Double getBGN() {
        return bGN;
    }

    public void setBGN(Double bGN) {
        this.bGN = bGN;
    }

    public Double getBRL() {
        return bRL;
    }

    public void setBRL(Double bRL) {
        this.bRL = bRL;
    }

    public Double getCAD() {
        return cAD;
    }

    public void setCAD(Double cAD) {
        this.cAD = cAD;
    }

    public Double getCHF() {
        return cHF;
    }

    public void setCHF(Double cHF) {
        this.cHF = cHF;
    }

    public Double getCNY() {
        return cNY;
    }

    public void setCNY(Double cNY) {
        this.cNY = cNY;
    }

    public Double getCZK() {
        return cZK;
    }

    public void setCZK(Double cZK) {
        this.cZK = cZK;
    }

    public Double getDKK() {
        return dKK;
    }

    public void setDKK(Double dKK) {
        this.dKK = dKK;
    }

    public Double getGBP() {
        return gBP;
    }

    public void setGBP(Double gBP) {
        this.gBP = gBP;
    }

    public Double getHKD() {
        return hKD;
    }

    public void setHKD(Double hKD) {
        this.hKD = hKD;
    }

    public Double getHRK() {
        return hRK;
    }

    public void setHRK(Double hRK) {
        this.hRK = hRK;
    }

    public Double getHUF() {
        return hUF;
    }

    public void setHUF(Double hUF) {
        this.hUF = hUF;
    }

    public Integer getIDR() {
        return iDR;
    }

    public void setIDR(Integer iDR) {
        this.iDR = iDR;
    }

    public Double getILS() {
        return iLS;
    }

    public void setILS(Double iLS) {
        this.iLS = iLS;
    }

    public Double getINR() {
        return iNR;
    }

    public void setINR(Double iNR) {
        this.iNR = iNR;
    }

    public Double getJPY() {
        return jPY;
    }

    public void setJPY(Double jPY) {
        this.jPY = jPY;
    }

    public Double getKRW() {
        return kRW;
    }

    public void setKRW(Double kRW) {
        this.kRW = kRW;
    }

    public Double getMXN() {
        return mXN;
    }

    public void setMXN(Double mXN) {
        this.mXN = mXN;
    }

    public Double getMYR() {
        return mYR;
    }

    public void setMYR(Double mYR) {
        this.mYR = mYR;
    }

    public Double getNOK() {
        return nOK;
    }

    public void setNOK(Double nOK) {
        this.nOK = nOK;
    }

    public Double getNZD() {
        return nZD;
    }

    public void setNZD(Double nZD) {
        this.nZD = nZD;
    }

    public Double getPHP() {
        return pHP;
    }

    public void setPHP(Double pHP) {
        this.pHP = pHP;
    }

    public Double getPLN() {
        return pLN;
    }

    public void setPLN(Double pLN) {
        this.pLN = pLN;
    }

    public Double getRON() {
        return rON;
    }

    public void setRON(Double rON) {
        this.rON = rON;
    }

    public Double getRUB() {
        return rUB;
    }

    public void setRUB(Double rUB) {
        this.rUB = rUB;
    }

    public Double getSEK() {
        return sEK;
    }

    public void setSEK(Double sEK) {
        this.sEK = sEK;
    }

    public Double getSGD() {
        return sGD;
    }

    public void setSGD(Double sGD) {
        this.sGD = sGD;
    }

    public Double getTHB() {
        return tHB;
    }

    public void setTHB(Double tHB) {
        this.tHB = tHB;
    }

    public Double getTRY() {
        return tRY;
    }

    public void setTRY(Double tRY) {
        this.tRY = tRY;
    }

    public Double getUSD() {
        return uSD;
    }

    public void setUSD(Double uSD) {
        this.uSD = uSD;
    }

    public Double getZAR() {
        return zAR;
    }

    public void setZAR(Double zAR) {
        this.zAR = zAR;
    }

    public ArrayList<String[]> getStrings(){
        ArrayList<String[]> arrayList = new ArrayList<>();
        String str[] = new String[2];
        /*
        "AUD": 1.4868,
        "BGN": 1.9558,
        "BRL": 3.7476,
        "CAD": 1.4867,
        "CHF": 1.0935,
        "CNY": 7.7412,
        "CZK": 26.3,
        "DKK": 7.4367,
        "GBP": 0.8799,
        "HKD": 8.9107,
        "HRK": 7.4125,
        "HUF": 310.06,
        "IDR": 15217,
        "ILS": 3.9894,
        "INR": 73.713,
        "JPY": 128.59,
        "KRW": 1304.1,
        "MXN": 20.47,
        "MYR": 4.9002,
        "NOK": 9.57,
        "NZD": 1.5651,
        "PHP": 57.706,
        "PLN": 4.2489,
        "RON": 4.5744,
        "RUB": 67.3,
        "SEK": 9.7215,
        "SGD": 1.5751,
        "THB": 38.787,
        "TRY": 4.0143,
        "USD": 1.1413,
        "ZAR": 14.826
         */
        if(bGN != null) {
            str = new String[2];
            str[0] = "BGN";
            str[1] = getBGN().toString();
            arrayList.add(str);
        }

        if(aUD != null) {
            str = new String[2];
            str[0] = "AUD";
            str[1] = getAUD().toString();
            arrayList.add(str);
        }

        if(bRL != null) {
            str = new String[2];
            str[0] = "BRL";
            str[1] = getBRL().toString();
            arrayList.add(str);
        }

        if(cAD != null) {
            str = new String[2];
            str[0] = "CAD";
            str[1] = getCAD().toString();
            arrayList.add(str);
        }

        if(cHF != null) {
            str = new String[2];
            str[0] = "CHF";
            str[1] = getCHF().toString();
            arrayList.add(str);
        }

        if(cNY != null) {
            str = new String[2];
            str[0] = "CNY";
            str[1] = getCNY().toString();
            arrayList.add(str);
        }

        if(cZK != null) {
            str = new String[2];
            str[0] = "CZK";
            str[1] = getCZK().toString();
            arrayList.add(str);
        }

        if(dKK != null) {
            str = new String[2];
            str[0] = "DKK";
            str[1] = getDKK().toString();
            arrayList.add(str);
        }

        if(gBP != null) {
            str = new String[2];
            str[0] = "GBP";
            str[1] = getGBP().toString();
            arrayList.add(str);
        }

        if(hKD != null) {
            str = new String[2];
            str[0] = "HKD";
            str[1] = getHKD().toString();
            arrayList.add(str);
        }

        if(hRK != null) {
            str = new String[2];
            str[0] = "HRK";
            str[1] = getHRK().toString();
            arrayList.add(str);
        }

        if(hUF != null) {
            str = new String[2];
            str[0] = "HUF";
            str[1] = getHUF().toString();
            arrayList.add(str);
        }

        if(iDR != null) {
            str = new String[2];
            str[0] = "IDR";
            str[1] = getIDR().toString();
            arrayList.add(str);
        }

        if(iLS != null) {
            str = new String[2];
            str[0] = "ILS";
            str[1] = getILS().toString();
            arrayList.add(str);
        }

        if(iNR != null) {
            str = new String[2];
            str[0] = "INR";
            str[1] = getINR().toString();
            arrayList.add(str);
        }

        if(jPY != null) {
            str = new String[2];
            str[0] = "JPY";
            str[1] = getJPY().toString();
            arrayList.add(str);
        }


        if(kRW != null) {
            str = new String[2];
            str[0] = "KRW";
            str[1] = getKRW().toString();
            arrayList.add(str);
        }

        if(mXN != null) {
            str = new String[2];
            str[0] = "MXN";
            str[1] = getMXN().toString();
            arrayList.add(str);
        }

        if(mYR != null) {
            str = new String[2];
            str[0] = "MYR";
            str[1] = getMYR().toString();
            arrayList.add(str);
        }

        if(nOK != null) {
            str = new String[2];
            str[0] = "NOK";
            str[1] = getNOK().toString();
            arrayList.add(str);
        }

        if(nZD != null) {
            str = new String[2];
            str[0] = "NZD";
            str[1] = getNZD().toString();
            arrayList.add(str);
        }

        if(pHP != null) {
            str = new String[2];
            str[0] = "PHP";
            str[1] = getPHP().toString();
            arrayList.add(str);
        }

        if(pLN != null) {
            str = new String[2];
            str[0] = "PLN";
            str[1] = getPLN().toString();
            arrayList.add(str);
        }

        if(rON != null) {
            str = new String[2];
            str[0] = "RON";
            str[1] = getRON().toString();
            arrayList.add(str);
        }

        if(rUB != null) {
            str = new String[2];
            str[0] = "RUB";
            str[1] = getRUB().toString();
            arrayList.add(str);
        }

        if(sEK != null) {
            str = new String[2];
            str[0] = "SEK";
            str[1] = getSEK().toString();
            arrayList.add(str);
        }

        if(sGD != null) {
            str = new String[2];
            str[0] = "SGD";
            str[1] = getSGD().toString();
            arrayList.add(str);
        }

        if(tHB != null) {
            str = new String[2];
            str[0] = "THB";
            str[1] = getTHB().toString();
            arrayList.add(str);
        }

        if(tRY != null) {
            str = new String[2];
            str[0] = "TRY";
            str[1] = getTRY().toString();
        }

        if(uSD != null) {
            str = new String[2];
            arrayList.add(str);
            str[0] = "USD";
            str[1] = getUSD().toString();
            arrayList.add(str);
        }

        if(zAR != null) {
            str = new String[2];
            str[0] = "ZAR";
            str[1] = getZAR().toString();
            arrayList.add(str);
        }


        return arrayList;
    }
}
